/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.sqlparser;

/**
 * SQL识别器接口
 */
public interface SQLRecognizer {

    /**
     * 获取SQL语句类型，例如：INSERT/UPDATE/DELETE ...
     */
    SQLType getSQLType();

    /**
     * 获取表别名，例如：SELECT id, name FROM user u WHERE ...；别名就应该是‘u’。
     */
    String getTableAlias();

    /**
     * 获取表名，例如：SELECT id, name FROM user u WHERE ...；表名为‘user’。
     */
    String getTableName();

    /**
     * 获取原始SQL语句
     */
    String getOriginalSQL();
}
