/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.rm.datasource.undo;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Set;

import io.seata.core.exception.TransactionException;
import io.seata.rm.datasource.ConnectionProxy;
import io.seata.rm.datasource.DataSourceProxy;

/**
 * 事务日志管理器
 */
public interface UndoLogManager {

    /**
     * 保存事务日志
     */
    void flushUndoLogs(ConnectionProxy cp) throws SQLException;

    /**
     * 二阶段【回滚】处理的undo操作
     */
    void undo(DataSourceProxy dataSourceProxy, String xid, long branchId) throws TransactionException;

    /**
     * 二阶段【回滚】处理的删除事务日志的操作
     */
    void deleteUndoLog(String xid, long branchId, Connection conn) throws SQLException;

    /**
     * 二阶段【提交】处理的批量删除事务日志的操作
     */
    void batchDeleteUndoLog(Set<String> xids, Set<Long> branchIds, Connection conn) throws SQLException;

    /**
     * 根据创建时间删除事务日志的操作
     */
    int deleteUndoLogByLogCreated(Date logCreated, int limitRows, Connection conn) throws SQLException;

}
