/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.rm.datasource.exec;

import io.seata.common.DefaultValues;
import io.seata.common.util.NumberUtils;
import io.seata.config.Configuration;
import io.seata.config.ConfigurationCache;
import io.seata.config.ConfigurationChangeEvent;
import io.seata.config.ConfigurationChangeListener;
import io.seata.config.ConfigurationFactory;
import io.seata.core.constants.ConfigurationKeys;
import io.seata.core.context.GlobalLockConfigHolder;
import io.seata.core.model.GlobalLockConfig;

/**
 * Lock retry controller
 *
 * @author sharajava
 */
public class LockRetryController {

    private static final GlobalConfig LISTENER = new GlobalConfig();

    /**
     * 将配置变量维护到Lisntener中
     */
    static {
        ConfigurationCache.addConfigListener(ConfigurationKeys.CLIENT_LOCK_RETRY_INTERVAL, LISTENER); // 重试间隔
        ConfigurationCache.addConfigListener(ConfigurationKeys.CLIENT_LOCK_RETRY_TIMES, LISTENER); // 重试次数
    }

    private int lockRetryInternal;

    private int lockRetryTimes;

    public LockRetryController() {
        this.lockRetryInternal = getLockRetryInternal();
        this.lockRetryTimes = getLockRetryTimes();
    }

    public void sleep(Exception e) throws LockWaitTimeoutException {
        /** lockRetryTimes减1，如果小于0，则直接抛异常 */
        if (--lockRetryTimes < 0) {
            throw new LockWaitTimeoutException("Global lock wait timeout", e);
        }

        try {
            /** 执行lockRetryInternal毫秒的睡眠 */
            Thread.sleep(lockRetryInternal);
        } catch (InterruptedException ignore) {
        }
    }

    /**
     * 获得重试间隔时间
     */
    int getLockRetryInternal() {
        /** 首先：获得定制的参数（保存在ThreadLocal中） */
        GlobalLockConfig config = GlobalLockConfigHolder.getCurrentGlobalLockConfig();
        if (config != null) {
            int configInternal = config.getLockRetryInternal();
            if (configInternal > 0) {
                return configInternal;
            }
        }

        /** 其次：如果没有配置定制参数，则使用全局变量配置 */
        return LISTENER.getGlobalLockRetryInternal();
    }

    /**
     * 获得重试次数
     */
    int getLockRetryTimes() {
        /** 首先：获得定制的参数（保存在ThreadLocal中） */
        GlobalLockConfig config = GlobalLockConfigHolder.getCurrentGlobalLockConfig();
        if (config != null) {
            int configTimes = config.getLockRetryTimes();
            if (configTimes >= 0) {
                return configTimes;
            }
        }

        /** 其次：如果没有配置定制参数，则使用全局变量配置 */
        return LISTENER.getGlobalLockRetryTimes();
    }

    static class GlobalConfig implements ConfigurationChangeListener {

        private volatile int globalLockRetryInternal;

        private volatile int globalLockRetryTimes;

        private final int defaultRetryInternal = DefaultValues.DEFAULT_CLIENT_LOCK_RETRY_INTERVAL;
        private final int defaultRetryTimes = DefaultValues.DEFAULT_CLIENT_LOCK_RETRY_TIMES;

        public GlobalConfig() {
            Configuration configuration = ConfigurationFactory.getInstance();
            globalLockRetryInternal = configuration.getInt(ConfigurationKeys.CLIENT_LOCK_RETRY_INTERVAL, defaultRetryInternal);
            globalLockRetryTimes = configuration.getInt(ConfigurationKeys.CLIENT_LOCK_RETRY_TIMES, defaultRetryTimes);
        }

        @Override
        public void onChangeEvent(ConfigurationChangeEvent event) {
            String dataId = event.getDataId();
            String newValue = event.getNewValue();
            if (ConfigurationKeys.CLIENT_LOCK_RETRY_INTERVAL.equals(dataId)) {
                globalLockRetryInternal = NumberUtils.toInt(newValue, defaultRetryInternal);
            }
            if (ConfigurationKeys.CLIENT_LOCK_RETRY_TIMES.equals(dataId)) {
                globalLockRetryTimes = NumberUtils.toInt(newValue, defaultRetryTimes);
            }
        }

        public int getGlobalLockRetryInternal() {
            return globalLockRetryInternal;
        }

        public int getGlobalLockRetryTimes() {
            return globalLockRetryTimes;
        }
    }
}