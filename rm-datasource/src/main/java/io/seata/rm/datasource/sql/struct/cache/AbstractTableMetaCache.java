/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.rm.datasource.sql.struct.cache;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import io.seata.common.exception.ShouldNeverHappenException;
import io.seata.common.util.StringUtils;
import io.seata.core.context.RootContext;
import io.seata.rm.datasource.sql.struct.TableMeta;
import io.seata.rm.datasource.sql.struct.TableMetaCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The type Table meta cache.
 *
 * @author sharajava
 */
public abstract class AbstractTableMetaCache implements TableMetaCache {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTableMetaCache.class);

    /** 缓存大小为100KB */
    private static final long CACHE_SIZE = 100000;

    /** 过期时间900秒 */
    private static final long EXPIRE_TIME = 900 * 1000;

    /** 表元数据缓存 */
    private static final Cache<String, TableMeta> TABLE_META_CACHE = Caffeine.newBuilder().maximumSize(CACHE_SIZE)
            .expireAfterWrite(EXPIRE_TIME, TimeUnit.MILLISECONDS).softValues().build();

    /**
     * 从缓存中获得表元数据信息TableMeta
     */
    @Override
    public TableMeta getTableMeta(final Connection connection, final String tableName, String resourceId) {
        if (StringUtils.isNullOrEmpty(tableName)) {
            throw new IllegalArgumentException("TableMeta cannot be fetched without tableName");
        }

        TableMeta tmeta;
        // eg1: 调用MysqlTableMetaCache.getCacheKey(...)
        /** 构建缓存key */
        final String key = getCacheKey(connection, tableName, resourceId);

        // eg1: key="jdbc:mysql://127.0.0.1:3306/seata.t_stock"
        /** cese1：如果缓存中存在，则从缓存中获取表元数据 */
        tmeta = TABLE_META_CACHE.get(key, mappingFunction -> {
            try {
                /** cese2：如果缓存中不存在，则从DB中获取表元数据 */
                return fetchSchema(connection, tableName);
            } catch (SQLException e) {
                LOGGER.error("get table meta of the table `{}` error: {}", tableName, e.getMessage(), e);
                return null;
            }
        });

        if (tmeta == null) {
            throw new ShouldNeverHappenException(String.format("[xid:%s]get table meta failed," +
                " please check whether the table `%s` exists.", RootContext.getXID(), tableName));
        }
        return tmeta;
    }

    /**
     * TableMeta的刷新操作
     */
    @Override
    public void refresh(final Connection connection, String resourceId) {
        ConcurrentMap<String, TableMeta> tableMetaMap = TABLE_META_CACHE.asMap();
        /** 遍历所有缓存的表元数据TableMeta */
        for (Map.Entry<String, TableMeta> entry : tableMetaMap.entrySet()) {
            String key = getCacheKey(connection, entry.getValue().getTableName(), resourceId);
            /** 通过key找到缓存中的TableMeta，执行SQL语句重新获得TableMeta，如果与缓存不一致，则进行表元数据的更新操作 */
            if (entry.getKey().equals(key)) {
                try {
                    // 获取最新的表元数据TableMeta
                    TableMeta tableMeta = fetchSchema(connection, entry.getValue().getTableName());
                    // 如果与缓存的tableMeta不一致，则进行表元数据的更新操作
                    if (!tableMeta.equals(entry.getValue())) {
                        TABLE_META_CACHE.put(entry.getKey(), tableMeta);
                        LOGGER.info("table meta change was found, update table meta cache automatically.");
                    }
                } catch (SQLException e) {
                    LOGGER.error("get table meta error:{}", e.getMessage(), e);
                }
            }
        }
    }

    /**
     * generate cache key
     *
     * @param connection the connection
     * @param tableName  the table name
     * @param resourceId the resource id
     * @return cache key
     */
    protected abstract String getCacheKey(Connection connection, String tableName, String resourceId);

    /**
     * get scheme from datasource and tableName
     *
     * @param connection the connection
     * @param tableName  the table name
     * @return table meta
     * @throws SQLException the sql exception
     */
    protected abstract TableMeta fetchSchema(Connection connection, String tableName) throws SQLException;

}
