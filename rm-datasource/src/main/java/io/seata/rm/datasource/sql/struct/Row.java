/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.rm.datasource.sql.struct;

import java.util.ArrayList;
import java.util.List;


/**
 * 一个Row对象表示表中的一行记录
 */
public class Row implements java.io.Serializable {

    private static final long serialVersionUID = 6532477221179419451L;

    /**
     * 字段列表
     */
    private List<Field> fields = new ArrayList<>();


    public Row() {
    }

    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }

    public void add(Field field) {
        fields.add(field);
    }

    /**
     * Primary keys list.
     *
     * @return the Primary keys list
     */
    public List<Field> primaryKeys() {
        List<Field> pkFields = new ArrayList<>();
        for (Field field : fields) {
            if (KeyType.PRIMARY_KEY == field.getKeyType()) {
                pkFields.add(field);
            }
        }
        return pkFields;
    }

    /**
     * Non-primary keys list.
     *
     * @return the non-primary list
     */
    public List<Field> nonPrimaryKeys() {
        List<Field> nonPkFields = new ArrayList<>();
        for (Field field : fields) {
            if (KeyType.PRIMARY_KEY != field.getKeyType()) {
                nonPkFields.add(field);
            }
        }
        return nonPkFields;
    }
}
