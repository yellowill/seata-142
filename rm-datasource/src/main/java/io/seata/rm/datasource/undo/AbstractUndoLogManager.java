/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.rm.datasource.undo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.seata.common.Constants;
import io.seata.common.util.CollectionUtils;
import io.seata.common.util.SizeUtil;
import io.seata.config.ConfigurationFactory;
import io.seata.core.compressor.CompressorFactory;
import io.seata.core.compressor.CompressorType;
import io.seata.core.constants.ClientTableColumnsName;
import io.seata.core.constants.ConfigurationKeys;
import io.seata.core.exception.BranchTransactionException;
import io.seata.core.exception.TransactionException;
import io.seata.rm.datasource.ConnectionContext;
import io.seata.rm.datasource.ConnectionProxy;
import io.seata.rm.datasource.DataSourceProxy;
import io.seata.rm.datasource.sql.struct.TableMeta;
import io.seata.rm.datasource.sql.struct.TableMetaCacheFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.seata.common.DefaultValues.DEFAULT_TRANSACTION_UNDO_LOG_TABLE;
import static io.seata.common.DefaultValues.DEFAULT_CLIENT_UNDO_COMPRESS_ENABLE;
import static io.seata.common.DefaultValues.DEFAULT_CLIENT_UNDO_COMPRESS_TYPE;
import static io.seata.common.DefaultValues.DEFAULT_CLIENT_UNDO_COMPRESS_THRESHOLD;
import static io.seata.core.exception.TransactionExceptionCode.BranchRollbackFailed_Retriable;

/**
 * @author jsbxyyx
 */
public abstract class AbstractUndoLogManager implements UndoLogManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractUndoLogManager.class);

    protected enum State {
        /**
         * This state can be properly rolled back by services
         */
        Normal(0),
        /**
         * This state prevents the branch transaction from inserting undo_log after the global transaction is rolled
         * back.
         */
        GlobalFinished(1);

        private int value;

        State(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    /** 获取配置项“client.undo.logTable”，默认值为undo_log */
    protected static final String UNDO_LOG_TABLE_NAME = ConfigurationFactory.getInstance().getConfig(
            ConfigurationKeys.TRANSACTION_UNDO_LOG_TABLE, DEFAULT_TRANSACTION_UNDO_LOG_TABLE);

    protected static final String SELECT_UNDO_LOG_SQL = "SELECT * FROM " + UNDO_LOG_TABLE_NAME + " WHERE "
            + ClientTableColumnsName.UNDO_LOG_BRANCH_XID + " = ? AND " + ClientTableColumnsName.UNDO_LOG_XID
            + " = ? FOR UPDATE";

    protected static final String DELETE_UNDO_LOG_SQL = "DELETE FROM " + UNDO_LOG_TABLE_NAME + " WHERE "
            + ClientTableColumnsName.UNDO_LOG_BRANCH_XID + " = ? AND " + ClientTableColumnsName.UNDO_LOG_XID + " = ?";

    protected static final boolean ROLLBACK_INFO_COMPRESS_ENABLE = ConfigurationFactory.getInstance().getBoolean(
            ConfigurationKeys.CLIENT_UNDO_COMPRESS_ENABLE, DEFAULT_CLIENT_UNDO_COMPRESS_ENABLE);

    protected static final CompressorType ROLLBACK_INFO_COMPRESS_TYPE =
            CompressorType.getByName(ConfigurationFactory.getInstance().getConfig(
                    ConfigurationKeys.CLIENT_UNDO_COMPRESS_TYPE, DEFAULT_CLIENT_UNDO_COMPRESS_TYPE));

    protected static final long ROLLBACK_INFO_COMPRESS_THRESHOLD =
            SizeUtil.size2Long(ConfigurationFactory.getInstance().getConfig(
                    ConfigurationKeys.CLIENT_UNDO_COMPRESS_THRESHOLD, DEFAULT_CLIENT_UNDO_COMPRESS_THRESHOLD));

    private static final ThreadLocal<String> SERIALIZER_LOCAL = new ThreadLocal<>();

    public static String getCurrentSerializer() {
        return SERIALIZER_LOCAL.get();
    }

    public static void setCurrentSerializer(String serializer) {
        SERIALIZER_LOCAL.set(serializer);
    }

    public static void removeCurrentSerializer() {
        SERIALIZER_LOCAL.remove();
    }

    /**
     * Delete undo log.
     *
     * @param xid      the xid
     * @param branchId the branch id
     * @param conn     the conn
     * @throws SQLException the sql exception
     */
    @Override
    public void deleteUndoLog(String xid, long branchId, Connection conn) throws SQLException {
        try (PreparedStatement deletePST = conn.prepareStatement(DELETE_UNDO_LOG_SQL)) {
            deletePST.setLong(1, branchId);
            deletePST.setString(2, xid);
            deletePST.executeUpdate();
        } catch (Exception e) {
            if (!(e instanceof SQLException)) {
                e = new SQLException(e);
            }
            throw (SQLException) e;
        }
    }

    /**
     * 批量删除undolog
     */
    @Override
    public void batchDeleteUndoLog(Set<String> xids, Set<Long> branchIds, Connection conn) throws SQLException {
        if (CollectionUtils.isEmpty(xids) || CollectionUtils.isEmpty(branchIds)) {
            return;
        }
        int xidSize = xids.size();
        int branchIdSize = branchIds.size();

        /** 拼装批量删除undolog的SQL语句 */
        String batchDeleteSql = toBatchDeleteUndoLogSql(xidSize, branchIdSize);
        try (PreparedStatement deletePST = conn.prepareStatement(batchDeleteSql)) {
            int paramsIndex = 1;
            for (Long branchId : branchIds) {
                deletePST.setLong(paramsIndex++, branchId); // 向预编译语句中设置分支id
            }
            for (String xid : xids) {
                deletePST.setString(paramsIndex++, xid); // 向预编译语句中设置事务id
            }

            /** 执行删除操作 */
            int deleteRows = deletePST.executeUpdate();
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("batch delete undo log size {}", deleteRows);
            }
        } catch (Exception e) {
            if (!(e instanceof SQLException)) {
                e = new SQLException(e);
            }
            throw (SQLException) e;
        }
    }

    /**
     * 拼装批量删除undolog的SQL语句
     */
    protected static String toBatchDeleteUndoLogSql(int xidSize, int branchIdSize) {
        /** 拼装SQL语句：DELETE FROM undo_log WHERE branch_id IN (?,?,?...) AND xid IN (?,?,?...)*/
        StringBuilder sqlBuilder = new StringBuilder(64);
        sqlBuilder.append("DELETE FROM ").append(UNDO_LOG_TABLE_NAME).append(" WHERE  ").append(
                ClientTableColumnsName.UNDO_LOG_BRANCH_XID).append(" IN ");
        appendInParam(branchIdSize, sqlBuilder); // 根据branchIdSize的数量，拼装'?'
        sqlBuilder.append(" AND ").append(ClientTableColumnsName.UNDO_LOG_XID).append(" IN ");
        appendInParam(xidSize, sqlBuilder); // 根据xidSize的数量，拼装'?'
        return sqlBuilder.toString();
    }

    /**
     * 该方法用于拼装size个'?'，作为SQL的预编译符号
     */
    protected static void appendInParam(int size, StringBuilder sqlBuilder) {
        sqlBuilder.append(" (");
        for (int i = 0; i < size; i++) {
            sqlBuilder.append("?");
            if (i < (size - 1)) {
                sqlBuilder.append(",");
            }
        }
        sqlBuilder.append(") ");
    }

    /**
     * 判断是否能执行取消操作，要求state=0
     */
    protected static boolean canUndo(int state) {
        return state == State.Normal.getValue();
    }

    protected String buildContext(String serializer, CompressorType compressorType) {
        Map<String, String> map = new HashMap<>();
        map.put(UndoLogConstants.SERIALIZER_KEY, serializer);
        map.put(UndoLogConstants.COMPRESSOR_TYPE_KEY, compressorType.name());
        return CollectionUtils.encodeMap(map);
    }

    /**
     * 将String类型转换为Map类型
     */
    protected Map<String, String> parseContext(String data) {
        /** 将String类型转换为Map类型 */
        return CollectionUtils.decodeMap(data);
    }

    /**
     * 保存事务日志
     */
    @Override
    public void flushUndoLogs(ConnectionProxy cp) throws SQLException {
        /** 获得连接上下文，来判断是否有事务日志 */
        ConnectionContext connectionContext = cp.getContext();
        if (!connectionContext.hasUndoLog()) {
            return; // 如果没有事务日志，则直接返回
        }

        String xid = connectionContext.getXid(); // 事务id
        long branchId = connectionContext.getBranchId(); // 分支id

        /** 构造分支事务日志实体类 */
        BranchUndoLog branchUndoLog = new BranchUndoLog();
        branchUndoLog.setXid(xid);
        branchUndoLog.setBranchId(branchId);
        branchUndoLog.setSqlUndoLogs(connectionContext.getUndoItems()); // 设置SQL undoLog

        /** 将分支事务日志编码为字节数组 */
        UndoLogParser parser = UndoLogParserFactory.getInstance(); // 获得事务日志解析类，默认为：JacksonUndoLogParser
        byte[] undoLogContent = parser.encode(branchUndoLog);
        CompressorType compressorType = CompressorType.NONE;

        /** 如果事务日志需要被压缩，则执行压缩操作 */
        if (needCompress(undoLogContent)) {
            compressorType = ROLLBACK_INFO_COMPRESS_TYPE; // 获取配置项“client.undo.compress.type”，默认值为：zip
            undoLogContent = CompressorFactory.getCompressor(compressorType.getCode()).compress(undoLogContent);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Flushing UNDO LOG: {}", new String(undoLogContent, Constants.DEFAULT_CHARSET));
        }

        /** 插入事务日志 */
        insertUndoLogWithNormal(xid, branchId, buildContext(parser.getName(), compressorType), undoLogContent,
                cp.getTargetConnection());
    }

    /**
     * 执行分支事务回滚操作
     */
    @Override
    public void undo(DataSourceProxy dataSourceProxy, String xid, long branchId) throws TransactionException {
        Connection conn = null;
        ResultSet rs = null;
        PreparedStatement selectPST = null;
        boolean originalAutoCommit = true;

        for (; ; ) {
            try {
                /** 获取一个普通数据库连接 */
                conn = dataSourceProxy.getPlainConnection();

                /** 在一个本地事务内完成分支事务的二阶段回滚，如果是自动提交，则将其自动提交关闭掉 */
                if (originalAutoCommit = conn.getAutoCommit()) {
                    conn.setAutoCommit(false);
                }

                /** 通过"SELECT * FROM undo_log WHERE branch_id = ? AND xid = ? FOR UPDATE;"，查找undolog */
                selectPST = conn.prepareStatement(SELECT_UNDO_LOG_SQL);
                selectPST.setLong(1, branchId);
                selectPST.setString(2, xid);
                rs = selectPST.executeQuery();

                boolean exists = false; // 是否存在undolog
                while (rs.next()) {
                    exists = true;
                    /** 获得记录中log_status列的值，如果state不等于0，则无法执行回滚操作 */
                    int state = rs.getInt(ClientTableColumnsName.UNDO_LOG_LOG_STATUS); // log_status
                    if (!canUndo(state)) {
                        if (LOGGER.isInfoEnabled()) {
                            LOGGER.info("xid {} branch {}, ignore {} undo_log", xid, branchId, state);
                        }
                        return;
                    }

                    /** 获得记录中context列的值，将String类型的contextString转换为Map类型的context */
                    String contextString = rs.getString(ClientTableColumnsName.UNDO_LOG_CONTEXT); // context
                    Map<String, String> context = parseContext(contextString);

                    /** 压缩rollback_info列的值，输出字节码数组 */
                    byte[] rollbackInfo = getRollbackInfo(rs);

                    /** 从context字段中获得key为serializer对应的value值，根据该值找到匹配的undolog解析器，对rollback_info字段值进行解析操作 */
                    String serializer = context == null ? null : context.get(UndoLogConstants.SERIALIZER_KEY);
                    UndoLogParser parser = serializer == null ? UndoLogParserFactory.getInstance() : UndoLogParserFactory.getInstance(serializer);
                    BranchUndoLog branchUndoLog = parser.decode(rollbackInfo); // 默认parser是JacksonUndoLogParser实例

                    try {
                        /** 将序列化器的名称缓存到ThreadLocal中 */
                        setCurrentSerializer(parser.getName());

                        /**
                         * 一行undo_log数据代表一个分支事务，即：业务侧的一个本地事务。
                         * 一个本地事务可能包含多个insert、delete、update语句，各自对应一个SQLUndoLog对象
                         */
                        List<SQLUndoLog> sqlUndoLogs = branchUndoLog.getSqlUndoLogs();
                        if (sqlUndoLogs.size() > 1) {
                            Collections.reverse(sqlUndoLogs); /** 事务的回滚操作是要从后向前依次进行的，所以要把原有sql语句顺序执行反向操作 */
                        }
                        for (SQLUndoLog sqlUndoLog : sqlUndoLogs) {
                            /** 获取表元数据 */
                            TableMeta tableMeta = TableMetaCacheFactory.getTableMetaCache(dataSourceProxy.getDbType()).
                                            getTableMeta(conn, sqlUndoLog.getTableName(), dataSourceProxy.getResourceId());
                            sqlUndoLog.setTableMeta(tableMeta);

                            /** 获取回滚执行器 */
                            AbstractUndoExecutor undoExecutor = UndoExecutorFactory.getUndoExecutor(dataSourceProxy.getDbType(), sqlUndoLog);

                            /** 执行回滚 */
                            undoExecutor.executeOn(conn);
                        }
                    } finally {
                        /** 删除保存的序列化器名称 */
                        removeCurrentSerializer();
                    }
                }

                if (exists) {
                    /** 删除undolog，并提交事务 */
                    deleteUndoLog(xid, branchId, conn);
                    conn.commit();
                    if (LOGGER.isInfoEnabled()) {
                        LOGGER.info("xid {} branch {}, undo_log deleted with {}", xid, branchId, State.GlobalFinished.name());
                    }
                } else {
                    /** 插入undolog，事务状态为1，即：全局事务提交成功。并提交事务 */
                    insertUndoLogWithGlobalFinished(xid, branchId, UndoLogParserFactory.getInstance(), conn);
                    conn.commit();
                    if (LOGGER.isInfoEnabled()) {
                        LOGGER.info("xid {} branch {}, undo_log added with {}", xid, branchId, State.GlobalFinished.name());
                    }
                }
                return;
            } catch (SQLIntegrityConstraintViolationException e) {
                // Possible undo_log has been inserted into the database by other processes, retrying rollback undo_log
                if (LOGGER.isInfoEnabled()) {
                    LOGGER.info("xid {} branch {}, undo_log inserted, retry rollback", xid, branchId);
                }
            } catch (Throwable e) {
                if (conn != null) {
                    try {
                        conn.rollback(); /** 如果发生异常，则回滚 */
                    } catch (SQLException rollbackEx) {
                        LOGGER.warn("Failed to close JDBC resource while undo ... ", rollbackEx);
                    }
                }
                throw new BranchTransactionException(BranchRollbackFailed_Retriable, String
                        .format("Branch session rollback failed and try again later xid = %s branchId = %s %s", xid,
                                branchId, e.getMessage()), e);

            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (selectPST != null) {
                        selectPST.close();
                    }
                    if (conn != null) {
                        if (originalAutoCommit) {
                            conn.setAutoCommit(true); /** 将自动提交打开 */
                        }
                        conn.close();
                    }
                } catch (SQLException closeEx) {
                    LOGGER.warn("Failed to close JDBC resource while undo ... ", closeEx);
                }
            }
        }
    }

    /**
     * insert uodo log when global finished
     *
     * @param xid           the xid
     * @param branchId      the branchId
     * @param undoLogParser the undoLogParse
     * @param conn          sql connection
     * @throws SQLException SQLException
     */
    protected abstract void insertUndoLogWithGlobalFinished(String xid, long branchId, UndoLogParser undoLogParser,
                                                            Connection conn) throws SQLException;

    /**
     * insert uodo log when normal
     *
     * @param xid            the xid
     * @param branchId       the branchId
     * @param rollbackCtx    the rollbackContext
     * @param undoLogContent the undoLogContent
     * @param conn           sql connection
     * @throws SQLException SQLException
     */
    protected abstract void insertUndoLogWithNormal(String xid, long branchId, String rollbackCtx,
                                                    byte[] undoLogContent,
                                                    Connection conn) throws SQLException;

    /**
     * 压缩rollback_info列的值，输出字节码数组
     */
    protected byte[] getRollbackInfo(ResultSet rs) throws SQLException {
        /** 获得记录中rollback_info列的值 */
        byte[] rollbackInfo = rs.getBytes(ClientTableColumnsName.UNDO_LOG_ROLLBACK_INFO);

        /** 获得记录中context列的值，并转换为Map类型 */
        String rollbackInfoContext = rs.getString(ClientTableColumnsName.UNDO_LOG_CONTEXT);
        Map<String, String> context = CollectionUtils.decodeMap(rollbackInfoContext);

        /** 从context中获得执行压缩的类型（即：key=compressorType的值） */
        CompressorType compressorType = CompressorType
                .getByName(context.getOrDefault(UndoLogConstants.COMPRESSOR_TYPE_KEY, CompressorType.NONE.name()));

        /** 根据上面确定的压缩类型compressorType，对rollback_info列的值进行压缩 */
        return CompressorFactory.getCompressor(compressorType.getCode()).decompress(rollbackInfo);
    }

    /**
     * 是否需要对事务日志内容进行压缩操作
     * <p>
     * ROLLBACK_INFO_COMPRESS_ENABLE：获取配置项“client.undo.compress.enable”，默认值为true
     * ROLLBACK_INFO_COMPRESS_THRESHOLD：获取配置项“client.undo.compress.threshold”，默认值为64k
     */
    protected boolean needCompress(byte[] undoLogContent) {
        return ROLLBACK_INFO_COMPRESS_ENABLE && undoLogContent.length > ROLLBACK_INFO_COMPRESS_THRESHOLD;
    }
}
