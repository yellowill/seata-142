/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.rm.datasource.undo.mysql;

import io.seata.common.exception.ShouldNeverHappenException;
import io.seata.common.util.CollectionUtils;
import io.seata.rm.datasource.ColumnUtils;
import io.seata.rm.datasource.sql.struct.Field;
import io.seata.rm.datasource.sql.struct.Row;
import io.seata.rm.datasource.sql.struct.TableRecords;
import io.seata.rm.datasource.undo.AbstractUndoExecutor;
import io.seata.rm.datasource.undo.SQLUndoLog;
import io.seata.sqlparser.util.JdbcConstants;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The type My sql undo delete executor.
 *
 * @author sharajava
 */
public class MySQLUndoDeleteExecutor extends AbstractUndoExecutor {

    /**
     * Instantiates a new My sql undo delete executor.
     *
     * @param sqlUndoLog the sql undo log
     */
    public MySQLUndoDeleteExecutor(SQLUndoLog sqlUndoLog) {
        super(sqlUndoLog);
    }

    /**
     * INSERT INTO a (x, y, z, pk) VALUES (?, ?, ?, ?)
     */
    private static final String INSERT_SQL_TEMPLATE = "INSERT INTO %s (%s) VALUES (%s)";

    /**
     * 针对delete语句，创建回滚SQL
     * Notice: PK is at last one.
     */
    @Override
    protected String buildUndoSQL() {
        /** 获得前镜像 */
        TableRecords beforeImage = sqlUndoLog.getBeforeImage();

        /** 获得前镜像的所有行，如果获取不到，则表明delete语句没有删除任何行，抛出异常 */
        List<Row> beforeImageRows = beforeImage.getRows();
        if (CollectionUtils.isEmpty(beforeImageRows)) {
            throw new ShouldNeverHappenException("Invalid UNDO LOG");
        }

        /** 取出第一行 */
        Row row = beforeImageRows.get(0);
        /** 获取该行所有的非主键列 */
        List<Field> fields = new ArrayList<>(row.nonPrimaryKeys());
        /** 将主键放入到fields的末尾 */
        fields.addAll(getOrderedPkList(beforeImage, row, JdbcConstants.MYSQL));


        /** 构建插入列名和插入值'?' */
        String insertColumns = fields.stream().map(field -> ColumnUtils.addEscape(field.getName(), JdbcConstants.MYSQL))
            .collect(Collectors.joining(", "));
        String insertValues = fields.stream().map(field -> "?").collect(Collectors.joining(", "));

        /** 拼装SQL类似：INSERT INTO [tableName] (x, y, z, pk) VALUES (?, ?, ?, ?) */
        return String.format(INSERT_SQL_TEMPLATE, sqlUndoLog.getTableName(), insertColumns, insertValues);
    }

    @Override
    protected TableRecords getUndoRows() {
        return sqlUndoLog.getBeforeImage();
    }
}
