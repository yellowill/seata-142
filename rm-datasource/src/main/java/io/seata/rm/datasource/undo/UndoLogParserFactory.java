/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.rm.datasource.undo;

import io.seata.common.loader.EnhancedServiceLoader;
import io.seata.common.util.CollectionUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 事务日志解析类工厂
 */
public class UndoLogParserFactory {

    private UndoLogParserFactory() {
    }

    /**
     * key=serializerName
     * value=UndoLogParser的实现类
     */
    private static final ConcurrentMap<String, UndoLogParser> INSTANCES = new ConcurrentHashMap<>();

    /**
     * 默认为jackson的undolog解析器，即：JacksonUndoLogParser
     */
    public static UndoLogParser getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private static class SingletonHolder {
        private static final UndoLogParser INSTANCE = getInstance(UndoLogConstants.DEFAULT_SERIALIZER);
    }

    /**
     * 通过入参name获取事务日志解析器（UndoLogParser）
     * 如果缓存中不存在，则通过SPI机制加载UndoLogParser的子类
     */
    public static UndoLogParser getInstance(String name) {
        return CollectionUtils.computeIfAbsent(INSTANCES, name, key -> EnhancedServiceLoader.load(UndoLogParser.class, name));
    }
}
