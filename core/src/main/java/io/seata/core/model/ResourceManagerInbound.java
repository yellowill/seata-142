/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.core.model;

import io.seata.core.exception.TransactionException;

/**
 * 对内请求：TC ——> RM
 * 该接口用来控制一个分支事务的提交或回滚
 */
public interface ResourceManagerInbound {

    /**
     * 提交一个分支事务（xid=事务id）
     */
    BranchStatus branchCommit(BranchType branchType,
                              String xid,
                              long branchId,
                              String resourceId,
                              String applicationData) throws TransactionException;

    /**
     * 回滚一个分支事务（xid=事务id）
     */
    BranchStatus branchRollback(BranchType branchType,
                                String xid,
                                long branchId,
                                String resourceId,
                                String applicationData) throws TransactionException;
}
