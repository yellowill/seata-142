/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.core.model;

import io.seata.core.exception.TransactionException;

/**
 * 对外请求：RM ——> TC
 * 该接口用于向事务协调器（TC）发送事务处理请求
 */
public interface ResourceManagerOutbound {

    /**
     * 注册分支事务，并返回branchId
     */
    Long branchRegister(BranchType branchType,
                        String resourceId,
                        String clientId,
                        String xid,
                        String applicationData,
                        String lockKeys) throws TransactionException;

    /**
     * 上报分支状态
     */
    void branchReport(BranchType branchType,
                      String xid,
                      long branchId,
                      BranchStatus status,
                      String applicationData) throws TransactionException;

    /**
     * 查询全局锁
     */
    boolean lockQuery(BranchType branchType,
                      String resourceId,
                      String xid,
                      String lockKeys) throws TransactionException;
}
