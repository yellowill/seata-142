/*
 *  Copyright 1999-2019 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package io.seata.core.model;

import java.util.Map;

/**
 * 资源管理接口类
 */
public interface ResourceManager extends ResourceManagerInbound, ResourceManagerOutbound {

    /**
     * 注册资源操作
     */
    void registerResource(Resource resource);

    /**
     * 取消注册资源操作
     */
    void unregisterResource(Resource resource);

    /**
     * 获得被管理的所有资源
     */
    Map<String, Resource> getManagedResources();

    /**
     * 获得分支类型BranchType（BranchType.AT,BranchType.TCC,BranchType.SAGA,BranchType.XA）
     */
    BranchType getBranchType();
}
